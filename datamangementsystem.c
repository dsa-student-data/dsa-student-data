#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 12
#define REGISTRATION_LENGTH 8
#define MAX_PROGRAM_CODE_LENGTH 5

struct Student {
    char name[MAX_NAME_LENGTH];
    char date_of_birth[DATE_LENGTH];
    char registration_number[REGISTRATION_LENGTH];
    char program_code[MAX_PROGRAM_CODE_LENGTH];
    float annual_tuition;
};

struct Student students[MAX_STUDENTS];
int num_students = 0;

void print_menu() {
    printf("\nMenu:\n");
    printf("1. Add a student\n");
    printf("2. View all students\n");
    printf("3. Update a student\n");
    printf("4. Delete a student\n");
    printf("5. Search student by registration number\n");
    printf("6. Sort students\n");
    printf("7. Export student records to CSV\n");
    printf("8. Exit\n");
}

int compare_by_name(const void *a, const void *b) {
    return strcmp(((struct Student*)a)->name, ((struct Student*)b)->name);
}

int compare_by_registration(const void *a, const void *b) {
    return strcmp(((struct Student*)a)->registration_number, ((struct Student*)b)->registration_number);
}

void add_student() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }

    struct Student new_student;

    printf("Enter student name: ");
    fgets(new_student.name, sizeof(new_student.name), stdin);
    new_student.name[strcspn(new_student.name, "\n")] = 0; // remove newline character

    printf("Enter date of birth (YYYY-MM-DD): ");
    fgets(new_student.date_of_birth, sizeof(new_student.date_of_birth), stdin);
    new_student.date_of_birth[strcspn(new_student.date_of_birth, "\n")] = 0; // remove newline character

    printf("Enter registration number: ");
    fgets(new_student.registration_number, sizeof(new_student.registration_number), stdin);
    new_student.registration_number[strcspn(new_student.registration_number, "\n")] = 0; // remove newline character

    printf("Enter program code: ");
    fgets(new_student.program_code, sizeof(new_student.program_code), stdin);
    new_student.program_code[strcspn(new_student.program_code, "\n")] = 0; // remove newline character

    printf("Enter annual tuition in UGX: ");
    scanf("%f", &new_student.annual_tuition);
    getchar(); // consume newline character

    students[num_students++] = new_student;

    printf("Student added successfully.\n");
}

void view_students() {
    if (num_students == 0) {
        printf("No students found.\n");
        return;
    }

    printf("List of students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d:\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].registration_number);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition in UGX:  UGX %.2f\n", students[i].annual_tuition);
    }
}

void update_student() {
    int index;
    printf("Enter the index of the student to update: ");
    scanf("%d", &index);
    getchar(); // consume newline character

    if (index < 1 || index > num_students) {
        printf("Invalid index.\n");
        return;
    }

    struct Student updated_student;

    printf("Enter new student name: ");
    fgets(updated_student.name, sizeof(updated_student.name), stdin);
    updated_student.name[strcspn(updated_student.name, "\n")] = 0; // remove newline character

    printf("Enter new date of birth (YYYY-MM-DD): ");
    fgets(updated_student.date_of_birth, sizeof(updated_student.date_of_birth), stdin);
    updated_student.date_of_birth[strcspn(updated_student.date_of_birth, "\n")] = 0; 

    printf("Enter new registration number: ");
    fgets(updated_student.registration_number, sizeof(updated_student.registration_number), stdin);
    updated_student.registration_number[strcspn(updated_student.registration_number, "\n")] = 0; 

    printf("Enter new program code: ");
    fgets(updated_student.program_code, sizeof(updated_student.program_code), stdin);
    updated_student.program_code[strcspn(updated_student.program_code, "\n")] = 0; 

    printf("Enter new annual tuition in UGX: ");
    scanf("%f", &updated_student.annual_tuition);
    getchar(); // consume newline character

    students[index - 1] = updated_student;
    printf("Student updated successfully.\n");
}

void delete_student() {
    int index;
    printf("Enter the index of the student to delete: ");
    scanf("%d", &index);
    getchar(); // consume newline character

    if (index < 1 || index > num_students) {
        printf("Invalid index.\n");
        return;
    }

    for (int i = index - 1; i < num_students - 1; i++) {
        students[i] = students[i + 1];
    }
    num_students--;
    printf("Student deleted successfully.\n");
}

void search_student_by_registration() {
    char reg_number[REGISTRATION_LENGTH];
    printf("Enter registration number to search: ");
    fgets(reg_number, sizeof(reg_number), stdin);
    reg_number[strcspn(reg_number, "\n")] = 0; // remove newline character

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, reg_number) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].date_of_birth);
            printf("Registration Number: %s\n", students[i].registration_number);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition:  UGX %.2f\n", students[i].annual_tuition);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found.\n", reg_number);
    }
}

void sort_students(int sort_option) {
    switch (sort_option) {
        case 1:
            qsort(students, num_students, sizeof(struct Student), compare_by_name);
            printf("Students sorted by name:\n");
            for (int i = 0; i < num_students; i++) {
                printf("%s\n", students[i].name);
            }
            break;
        case 2:
            qsort(students, num_students, sizeof(struct Student), compare_by_registration);
            printf("Students sorted by registration number:\n");
            for (int i = 0; i < num_students; i++) {
                printf("%s\n", students[i].registration_number);
            }
            break;
        default:
            printf("Invalid sort option.\n");
            break;
    }
}

void export_to_csv() {
    FILE *file = fopen("student_records.csv", "w");
    if (file == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    fprintf(file, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    for (int i = 0; i < num_students; i++) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].date_of_birth, students[i].registration_number, students[i].program_code, students[i].annual_tuition);
    }

    fclose(file);
    printf("Student records exported to CSV successfully.\n");
}

int main() {
    int choice;
     int sort_option; // Move the declaration outside the switch block


    do {
        print_menu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // consume newline character

        switch (choice) {
            case 1:
                add_student();
                break;
            case 2:
                view_students();
                break;
            case 3:
                update_student();
                break;
            case 4:
                delete_student();
                break;
            case 5:
                search_student_by_registration();
                break;
            case 6:
                printf("Select sorting option:\n");
                printf("1. Sort by name\n");
                printf("2. Sort by registration number\n");
                printf("Enter your choice: ");
                scanf("%d", &sort_option);
                getchar(); // consume newline character
                sort_students(sort_option);
                break;
            case 7:
                export_to_csv();
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
                break;
        }
    } while (choice != 8);

    return 0;
}
