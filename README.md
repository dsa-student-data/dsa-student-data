# DSA Student data



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dsa-student-data/dsa-student-data.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/dsa-student-data/dsa-student-data/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
Student Data Management System

## Description
Student Data Management System is a command-line program designed to manage student records. It allows users to perform various operations such as adding, viewing, updating, and deleting student records. Additionally, it provides functionality for exporting student records to a CSV (Comma-Separated Values) file.

## Youtube Videos
https://youtu.be/QkDjrQj6nHI                        MAYANJA DANIEL
https://youtu.be/N2yNej96B_M                        MULIIKA EDRINE
https://youtu.be/vbit_J_96-Y?si=DM72JWtvqDkPc_Ki    TINA MARION ABWOR
https://youtu.be/P7LcvouLnlg                        NALUWAGA JOY DRICLLA
https://youtu.be/3ViOYcOIY6U                        BIYO  STELLA

### Data Structure Selection

The choice of data structure is crucial for the performance and scalability of the system. In this project, we have selected an array-based data structure to store student records. This decision was made based on the following considerations:

- **Efficiency**: Arrays provide constant-time access to elements, which is essential for quickly retrieving student records.
- **Simplicity**: Arrays are straightforward to implement and understand, making the codebase more maintainable.
- **Static Size**: Since the maximum number of students is predefined (MAX_STUDENTS), an array offers a fixed-size storage solution, preventing dynamic memory allocation overhead.
-**Efficient for Iteration**:Arrays are effiecient for iterating ince each element can be directly accessed directly by its index.
-**Predictable Performance**:Accessing elements in an array is generally predictable and consistent, making it easier to reason about performane of algorithms.
-**Efficient for Fixed size Data**: Arrays are ideal when the size of the data set is known and fixed, as they provide constant time access to any element.

By leveraging an array-based data structure, we ensure that the Student Data Management System remains efficient, reliable, and easy to maintain.
## Features
- Add new students with details including name, date of birth, registration number, program code, and annual tuition.
- View all existing student records.
- Update existing student records.
- Delete student records.
- Search for a student by their registration number.
- Sort students by name or registration number.
- Export student records to a CSV file for further analysis.

## Installation

To use the DSA Student Data Management System, follow these steps:

1. Clone the repository:
git clone https://github.com/your-username/dsa-student-data.git
2. Navigate to the project directory:
cd dsa-student-data
3. Compile the source code:
gcc main.c -o dsa_student_data
4. Run the program:
./dsa_student_data

## Authors and acknowledgment
MAYANJA DANIEL
NALUWAGA JOY D
BIYO STELLA
MULIIKA EDRINE
ABWOR TINA MARION

## Usage
Upon running the program, you will be presented with a menu containing various options. Choose an option by entering the corresponding number and follow the prompts to perform the desired operation. For example, to add a new student, select option 1 and enter the student's details as prompted.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
